ASM := nasm

FILE_NAME := test
SRC_DIR := source
BUILD_DIR := build


$(BUILD_DIR)/main.bin: $(SRC_DIR)/$(FILE_NAME).asm
	$(ASM) $(SRC_DIR)/$(FILE_NAME).asm -f bin -o $(BUILD_DIR)/main.bin
	cp $(BUILD_DIR)/main.bin $(BUILD_DIR)/main_floppy.img
	truncate -s 1440k $(BUILD_DIR)/main_floppy.img

